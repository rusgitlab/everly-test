<?php

/**
 * Class Path
 */
class Path
{
    const DIR_SEPARATOR = '/';
    const DIR_UP_SYMBOL = '..';

    public $currentPathParts = [];

    public $currentPath;

    /**
     * Path constructor.
     * @param string $path |
     */
    public function __construct(string $path)
    {
        $path = trim($path, self::DIR_SEPARATOR);

        $this->validateInitialPath($path);
        $this->parsePath($path);
    }

    /**
     * @param string $path | can contain .. and will lead to root if begins with /
     * @return $this
     */
    public function cd(string $path): Path
    {
        $firstSymbol = $path[0];
        $path = trim($path, self::DIR_SEPARATOR);

        $this->validateCDPath($path);

        $cdPath = explode(self::DIR_SEPARATOR, $path);

        $currentPathRaw = $firstSymbol === self::DIR_SEPARATOR
            ? $cdPath
            : array_merge($this->currentPathParts, $cdPath);

        //process '..' entries
        $currentPath = array_filter($currentPathRaw, function($index) use($currentPathRaw){
            return !(
                $currentPathRaw[$index] === self::DIR_UP_SYMBOL
                || isset($currentPathRaw[$index + 1]) && $currentPathRaw[$index + 1] === self::DIR_UP_SYMBOL
            );
        }, ARRAY_FILTER_USE_KEY);
        $currentPathParts = array_values($currentPath);

        $this->currentPathParts = $currentPathParts;
        $currentPath = implode(self::DIR_SEPARATOR, $currentPathParts);
        $this->currentPath = self::DIR_SEPARATOR . $currentPath;

        return $this;
    }

    /**
     * @param string $path
     */
    protected function validateInitialPath(string $path): void
    {
        if(preg_match('/([^A-Za-z\/])+/', $path) || preg_match('/\/{2}/', $path)) {
            echo 'Invalid path' . PHP_EOL;
            exit();
        }
    }

    /**
     * @param string $path
     */
    protected function validateCDPath(string $path): void
    {
        if(
            preg_match('/([^A-Za-z\/.])+/', $path)
            || preg_match('/\/{2}/', $path)
            || preg_match('/\/\.[^.]/', $path)
            || preg_match('/\/\.[^.]\//', $path)
            || preg_match('/^\.[^.]/', $path)
            || preg_match('/[^.]\.$/', $path)
            || preg_match('/\.[^.]$/', $path)
            || preg_match('/[^.]\.[^.]/', $path)
            || preg_match('/\.{3}/', $path)
        ) {
            echo 'No such file or directory' . PHP_EOL;
            exit();
        }
    }

    /**
     * @param string $path
     */
    protected function parsePath(string $path): void
    {
        $this->currentPath = strlen($path) > 0 ? $path : self::DIR_SEPARATOR;
        $this->currentPathParts = strlen($path) > 0 ? explode(self::DIR_SEPARATOR, $path) : [];
    }
}

echo (new Path('/a/b/'))->cd('/d/../e/')->currentPath . PHP_EOL;




?>
