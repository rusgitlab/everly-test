function convertToBinary(decimal) {
  if (decimal === 0 || decimal === 1) return decimal;
  let binary = '';
  let x;

  do {
    x = decimal % 2;
    decimal = Math.floor(decimal / 2);
    binary = x + binary;
  } while(decimal > 1)

  binary = x + binary;
  return binary;
}

function reverseString(str) {
  return str.split("").reverse().join("");
}

function convertToDecimal(binary) {
  if (binary === 0 || binary === 1) return binary;
  let highhestDigitIndex = binary.length - 1
  let result = 0;
  for($i = highhestDigitIndex; $i >= 0; $i--) {
    let currentDigit = binary[highhestDigitIndex - $i];
    let subResult = currentDigit * Math.pow(2, $i);
    result += subResult
  }
  return result;
}

function getReversedViaBinary(number) {
  let binary = convertToBinary(number);
  let reversedBinary = binary.length > 1 ? reverseString(binary) : binary;
  let reversedDecimal = convertToDecimal(reversedBinary);
  return reversedDecimal;
}

console.log(
    getReversedViaBinary(13)
);