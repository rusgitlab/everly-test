import math

def distance(lat1, lng1, lat2, lng2):
    pi80 = math.pi / 180
    lat1 *= pi80
    lng1 *= pi80
    lat2 *= pi80
    lng2 *= pi80

    r = 6372.797
    dlat = lat2 - lat1
    dlng = lng2 - lng1
    a = math.sin(dlat / 2) * math.sin(dlat / 2) + math.cos(lat1) * math.cos(lat2) * math.sin(dlng / 2) * math.sin(dlng / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    km = r * c

    return km

locations = [
    {'id': 1000, 'zip_code': '37069', 'lat': 45.35, 'lng': 10.84},
    {'id': 1001, 'zip_code': '37121', 'lat': 45.44, 'lng': 10.99},
    {'id': 1001, 'zip_code': '37129', 'lat': 45.44, 'lng': 11.00},
    {'id': 1001, 'zip_code': '37133', 'lat': 45.43, 'lng': 11.02}
]

shoppers = [
    {'id': 'S1', 'lat': 45.46, 'lng': 11.03, 'enabled': True},
    {'id': 'S2', 'lat': 45.46, 'lng': 10.12, 'enabled': True},
    {'id': 'S3', 'lat': 45.34, 'lng': 10.81, 'enabled': True},
    {'id': 'S4', 'lat': 45.76, 'lng': 10.57, 'enabled': True},
    {'id': 'S5', 'lat': 45.34, 'lng': 10.63, 'enabled': True},
    {'id': 'S6', 'lat': 45.42, 'lng': 10.81, 'enabled': True},
    {'id': 'S7', 'lat': 45.34, 'lng': 10.94, 'enabled': True}
]

for location in locations:
    shoppersCoverageData = []

    coverageData = []
    oneLocationPercentage = 100 / len(locations)
    distanceToCountCovered = 10

    for shopper in shoppers:
        coverageData = {'shopper_id': shopper['id'], 'coverage': 0}
        if shopper['enabled']:
            for location in locations:
                if (
                    distance(
                        location['lat'],
                        location['lng'],
                        shopper['lat'],
                        shopper['lng']
                    )
                    <=
                    distanceToCountCovered
                ):
                    coverageData['coverage'] += oneLocationPercentage
        shoppersCoverageData.append(coverageData)

print sorted(shoppersCoverageData, key = lambda i: i['coverage'],reverse=True)